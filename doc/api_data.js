define({ "api": [
  {
    "type": "post",
    "url": "api/v1/signin/adv",
    "title": "Регистрация рекламодателя",
    "version": "1.0.0",
    "name": "Authorization",
    "group": "Advertiser",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Почтовый ящик пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1    500\n{\n    \"status\": \"error\",\n    \"error\": [Описание ошибки]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1    201\n{\n    \"token_type\": \"Bearer\",\n    \"expires_in\": 86400,\n    \"access_token\": \"eyJ0eXA...\",\n    \"refresh_token\": \"def502...\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/AuthController.php",
    "groupTitle": "Advertiser"
  },
  {
    "type": "post",
    "url": "api/v1/signup/adv",
    "title": "Регистрация рекламодателя",
    "version": "1.0.0",
    "name": "Registration",
    "group": "Advertiser",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Имя пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Почтовый ящик пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": "<p>Повтор пароля</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1    500\n{\n    \"status\": \"error\",\n    \"errors\": [Список возникших ошибок]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1    201\n{\n    \"status\": \"success\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/AuthController.php",
    "groupTitle": "Advertiser"
  },
  {
    "type": "post",
    "url": "/api/oauth/token",
    "title": "Авторизация пользователя",
    "version": "1.0.0",
    "name": "GetToken",
    "group": "OAuth",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Requested-With",
            "description": "<p>XMLHttpRequest</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "form_params",
            "description": "<p>Данные формы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.grant_type",
            "description": "<p>Тип авториазции</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.client_id",
            "description": "<p>id клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.username",
            "description": "<p>Почтовый ящик пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.password",
            "description": "<p>Пароль пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "form_params.scope",
            "description": "<p>Разрешения API</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.guard",
            "description": "<p>Guard для определения таблицы аутентификации</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Тип токена</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Окончание действия токена в секундах</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Токен доступа</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Токен обновления</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный ответ:",
          "content": "HTTP/1.1 200 OK\n{\n    \"token_type\": \"Bearer\",\n    \"expires_in\": 86400,\n    \"access_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYzZmMDYwODAzNzQ3MThkOGIxZThkZTAzZTgyNjg4Zjg0OGUxNTk3Yjg1MThkYWFmMmE2ZTc2NTk3NjExYWQ0NDUzMDY0Zjg0NDI5NWE0YjYiLCJpYXQiOjE1ODQzNzQxMzMsIm5iZiI6MTU4NDM3NDEzMywiZXhwIjoxNTg0NDYwNTMzLCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.pNwFpVJUtknYpVN-CzZday2hdIUa1CG7J0uKy_l0zbbrz1ls1Hhel73p8dH-LKLbL2qf58UGPFAmLEG5dXuzvkuXgY5AcrqKad50gECtD9GnF7bnkL0V_aw70NoqNy5z0Kxr6h4MXWbmFGf1UZ9-bl-VCQhhAH_YWLQCgr8we3XCxop88OCrFC-GHOJOt45A37NfaGTz4sydB7bhu2QWe7crNorJK1bylWL4wM8aqG7dCw0gmQ8hpmKxWIMHKXuYc9Uzdiby2C4U0p8qTRHifb74KPgAEqH-T3JPUbHEyADX_N_DkehB7zeGsykNb6hK2ZsoNZja4blvHE2I16os0fVwhWUVrtCKHqXIjdgWwlDlo17DNSwlcRHZfhr2D5PjYk2YXstc-liN6VNQLOpI6Ar-6hevCOEDetntLhdg9KUUyy8T9s1trkpCz-nybp92BiFWmKEtoxXQ3KdMNe0EBaIxFHRv0COKw9WVe1WZJRCk2124gYbpGUCurc50VvQGESUvMqKILpnc8iiVJteLuhqPe4412xQQUKfTGJE8fBTeN8pbIaQKYjBbU78BliTIjxMVlGfA93uI5LMWztPV2-HqgqOn652ukzkzVDbjeKUd1QMF61pkUfxGHca6vsQSZRO3a13UnPRg3TPerN7_u62vYnP0mFNUaNPIU9XJfl8\",\n    \"refresh_token\": \"def5020086594e12caf911ef8a374ba0b6d88c57e5f4d67084b2d4128d8308c661936f2e472cbbddcfe171a19764958efa2bd45a04d55128b1d0794031b69be8659968e9e6b203383c4ace40470e1f10ab8e8763cdaafeb5cfdf3febb61c85d0b6c9900e63f83d1f88b067cbfbf2274c70dd37252cf864bb3063f5319d1710527af5967e674746fa6130c552d37861c3fc8032368c4848d1727effb33da1bf92e094058bc117fd16a3a9bbb49f4b893b101fbbd25c3f96fe4ee1fbfe7bd12f6594314ff88c14a1853d8b25d6ba6fefdb7a2fd7b153230545f7b1e00c2587a4492a4f5e828ceeb423647fc930d56a2c482286ca3cbd5d57ccda965f749e8eef3d924c3d1918150bfef8649c59e3b39da1d5cd1040a8632eba00a4ce33c21566762b4aebada2d0ea12abe6adfe831711721f979c1bcd8c6e466edd81912ab07f29346227f7ef58f2f2f9afe70587280b8d981705f03b0f9784564230e4a95c374e99090fad\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Неуспешный ответ:",
          "content": "HTTP/1.1 400 OK\n{\n    \"error\": \"invalid_grant\",\n    \"error_description\": \"The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.\",\n    \"hint\": \"\",\n    \"message\": \"The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Providers/AuthServiceProvider.php",
    "groupTitle": "OAuth"
  },
  {
    "type": "post",
    "url": "/api/oauth/token",
    "title": "Обновление токена",
    "version": "1.0.0",
    "name": "RefreshToken",
    "group": "OAuth",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Requested-With",
            "description": "<p>XMLHttpRequest</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "form_params",
            "description": "<p>Данные формы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.grant_type",
            "description": "<p>Тип авториазции</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.refresh_token",
            "description": "<p>Токен обновления</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.client_id",
            "description": "<p>id клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "form_params.client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "form_params.scope",
            "description": "<p>Разрешения API</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Успешный ответ:",
          "content": "HTTP/1.1 200 OK\n{\n    \"token_type\": \"Bearer\",\n    \"expires_in\": 86400,\n    \"access_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYzZmMDYwODAzNzQ3MThkOGIxZThkZTAzZTgyNjg4Zjg0OGUxNTk3Yjg1MThkYWFmMmE2ZTc2NTk3NjExYWQ0NDUzMDY0Zjg0NDI5NWE0YjYiLCJpYXQiOjE1ODQzNzQxMzMsIm5iZiI6MTU4NDM3NDEzMywiZXhwIjoxNTg0NDYwNTMzLCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.pNwFpVJUtknYpVN-CzZday2hdIUa1CG7J0uKy_l0zbbrz1ls1Hhel73p8dH-LKLbL2qf58UGPFAmLEG5dXuzvkuXgY5AcrqKad50gECtD9GnF7bnkL0V_aw70NoqNy5z0Kxr6h4MXWbmFGf1UZ9-bl-VCQhhAH_YWLQCgr8we3XCxop88OCrFC-GHOJOt45A37NfaGTz4sydB7bhu2QWe7crNorJK1bylWL4wM8aqG7dCw0gmQ8hpmKxWIMHKXuYc9Uzdiby2C4U0p8qTRHifb74KPgAEqH-T3JPUbHEyADX_N_DkehB7zeGsykNb6hK2ZsoNZja4blvHE2I16os0fVwhWUVrtCKHqXIjdgWwlDlo17DNSwlcRHZfhr2D5PjYk2YXstc-liN6VNQLOpI6Ar-6hevCOEDetntLhdg9KUUyy8T9s1trkpCz-nybp92BiFWmKEtoxXQ3KdMNe0EBaIxFHRv0COKw9WVe1WZJRCk2124gYbpGUCurc50VvQGESUvMqKILpnc8iiVJteLuhqPe4412xQQUKfTGJE8fBTeN8pbIaQKYjBbU78BliTIjxMVlGfA93uI5LMWztPV2-HqgqOn652ukzkzVDbjeKUd1QMF61pkUfxGHca6vsQSZRO3a13UnPRg3TPerN7_u62vYnP0mFNUaNPIU9XJfl8\",\n    \"refresh_token\": \"def5020086594e12caf911ef8a374ba0b6d88c57e5f4d67084b2d4128d8308c661936f2e472cbbddcfe171a19764958efa2bd45a04d55128b1d0794031b69be8659968e9e6b203383c4ace40470e1f10ab8e8763cdaafeb5cfdf3febb61c85d0b6c9900e63f83d1f88b067cbfbf2274c70dd37252cf864bb3063f5319d1710527af5967e674746fa6130c552d37861c3fc8032368c4848d1727effb33da1bf92e094058bc117fd16a3a9bbb49f4b893b101fbbd25c3f96fe4ee1fbfe7bd12f6594314ff88c14a1853d8b25d6ba6fefdb7a2fd7b153230545f7b1e00c2587a4492a4f5e828ceeb423647fc930d56a2c482286ca3cbd5d57ccda965f749e8eef3d924c3d1918150bfef8649c59e3b39da1d5cd1040a8632eba00a4ce33c21566762b4aebada2d0ea12abe6adfe831711721f979c1bcd8c6e466edd81912ab07f29346227f7ef58f2f2f9afe70587280b8d981705f03b0f9784564230e4a95c374e99090fad\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Неуспешный ответ:",
          "content": "HTTP/1.1 401 OK\n {\n     \"error\": \"invalid_request\",\n     \"error_description\": \"The refresh token is invalid.\",\n     \"hint\": \"Token has been revoked\",\n     \"message\": \"The refresh token is invalid.\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Providers/AuthServiceProvider.php",
    "groupTitle": "OAuth"
  }
] });
