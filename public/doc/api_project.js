define({
  "name": "Affiliate Doc",
  "version": "1.0.0",
  "description": "apiDoc for Affiliate System",
  "title": "apiDoc documentation",
  "url": "http://api.affiliate.test",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-06-04T12:08:10.936Z",
    "url": "http://apidocjs.com",
    "version": "0.20.1"
  }
});
