<?php

namespace App\Models;

use App\Classes\Traits\TranslationTrait;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use TranslationTrait;

    protected $table = 'offers';
    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $this->translatable = ['name'];
        $this->translatableType = 'Offers';

        parent::__construct($attributes);
    }
}
