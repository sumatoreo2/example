<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель для морф связи переведенных полей
 *
 * Class Translation
 * @package App\Models
 * @property int $id
 * @property string $field
 * @property string $lang
 * @property string $value
 * @property string $translation_type
 * @property int $translation_id
 * @property-read int $created_at
 * @property-read int $updated_at
 */
class Translation extends Model
{
    protected $table = 'translations';
    protected $guarded = ['id'];
}
