<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Classes\Traits\TranslationTrait;

/**
 * Рекламный продукт
 *
 * Class Product
 * @package App\Models
 * @property int $owner_id
 * @property string $url
 * @property int $status
 * @property-read int $created_at
 * @property-read int $updated_at
 */
class Product extends Model
{
    use TranslationTrait;

    protected $table = 'products';
    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $this->translatable = ['name', 'description'];
        $this->translatableType = 'Products';

        parent::__construct($attributes);
    }
}
