<?php

namespace App\Console\Commands;

use App\Classes\Repositories\Criteria\Products\StatusOn;
use App\Classes\Repositories\ProductRepository;
use App\Models\Offer;
use App\Models\Product;
use Faker\Factory;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function repositories() {
        for($i = 0; $i <= 20; $i++) {
            Product::create([
                'owner_id' => (Factory::create())->randomDigit,
                'url' => (Factory::create())->url,
                'status' => (Factory::create())->numberBetween(0,1)
            ]);

            $this->info('Create Product');
        }

        /** @var ProductRepository $product */
        $product = resolve(ProductRepository::class);

        $res = $product->pushCriteria(new StatusOn());

        dd($res->all()->toArray());
    }

    protected function translable() {
        app()->setLocale('ru');
        $offer = Offer::create();
        $offer->name = 'Название оффера';
        $offer->save();

        app()->setLocale('en');
        $offer->name = 'Offer name';
        $offer->save();

        foreach (['ru', 'en'] as $lang) {
            app()->setLocale($lang);
            $this->info(print_r([
                'lang' => $lang,
                'name' => $offer->name
            ], true));
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $this->repositories();
        $this->translable();
    }

}
