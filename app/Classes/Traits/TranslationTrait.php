<?php


namespace App\Classes\Traits;


use App\Models\Translation;
use Illuminate\Support\Facades\DB;

/**
 * Trait TranslationTrait
 * @package App\Classes\Traits
 * @property array $translatable
 * @property string $translatableType
 */
trait TranslationTrait
{
    /**
     * Список локализованных полей
     * @var array
     */
    protected $translatable = [];

    /**
     * Тим модели для разаграничения при записи
     * @var null
     */
    protected $translatableType = null;

    /**
     * Перегруженный метод с обращением к таблице переводов
     *
     * @param $key
     * @return mixed|null
     */
    public function __get($key)
    {
        if (in_array($key, $this->translatable)) {
            $field = DB::table('translations')->where([
                ['field', '=', $key],
                ['lang', '=', app()->getLocale()],
                ['translation_type', '=', $this->translatableType],
                ['translation_id', '=', $this->id]
            ])->first();

            return $field->value ?? null;
        } else {
            return $this->getAttribute($key);
        }
    }

    /**
     * Перегруженный метод с обращением к таблице переводов
     *
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        if (in_array($key, $this->translatable)) {
            Translation::updateOrCreate([
                'field' => $key,
                'lang' => app()->getLocale(),
                'translation_type' => $this->translatableType,
                'translation_id' => $this->id
            ], [
                'value' => $value
            ]);
        } else {
            $this->setAttribute($key, $value);
        }
    }
}