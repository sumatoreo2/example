<?php


namespace App\Classes\Repositories;



use App\Classes\Repositories\Eloquent\Repository;
use App\Models\Product;

/**
 * Репозиторий продукта
 *
 * Class ProductRepository
 * @package App\Classes\Repositories
 */
class ProductRepository extends Repository
{

    /**
     * Класс модели продукта
     *
     * @return string
     */
    function model()
    {
        return Product::class;
    }
}