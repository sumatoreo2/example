<?php


namespace App\Classes\Repositories\Eloquent;

use App\Classes\Repositories\Contracts\CriteriaInterface;
use App\Classes\Repositories\Contracts\RepositoryInterface;
use App\Classes\Repositories\Criteria\Criteria;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class Repository implements RepositoryInterface, CriteriaInterface
{
    private $app;

    /**
     * @var Model
     */
    protected $model;

    protected $criteria;

    protected $skipCriteria = false;

    public function __construct(App $app, Collection $collection)
    {
        $this->app = $app;
        $this->criteria = $collection;
        $this->resetScope();
        $this->makeModel();
    }

    abstract function model();

    public function all($columns = ['*']) {
        $this->applyCriteria();
        return $this->model->get($columns);
    }

    public function paginate($perPage = 15, $columns = ['*']) {
        $this->applyCriteria();
        return $this->model->paginate($perPage, $columns);
    }

    public function update(array $data, $id, $attr = 'id') {
        return $this->model->where($attr, '=', $id)->update($data);
    }

    public function delete($id) {
        return $this->model->destroy($id);
    }

    public function find($id, $columns = ['*']) {
        $this->applyCriteria();
        return $this->model->find($id, $columns);
    }

    public function findBy($attr, $value, $columns = ['*']) {
        $this->applyCriteria();
        return $this->model->where($attr, '=', $value)->first($columns);
    }

    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            //
        }

        return $this->model = $model;
    }

    public function resetScope() {
        $this->skipCriteria(false);

        return $this;
    }

    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;

        return $this;
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getByCriteria(Criteria $criteria)
    {
        $this->model = $criteria->apply($this->model, $this);

        return $this;
    }

    public function pushCriteria(Criteria $criteria)
    {
        $this->criteria->push($criteria);

        return $this;
    }

    public function applyCriteria()
    {
        if ($this->skipCriteria) {
            return $this;
        }

        foreach ($this->getCriteria() as $criteria) {
            if ($criteria instanceof Criteria) {
                $this->model = $criteria->apply($this->model, $this);
            }
        }

        return $this;
    }


}