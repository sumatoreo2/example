<?php


namespace App\Classes\Repositories;


use App\Classes\Repositories\Eloquent\Repository;
use App\Models\Offer;

class OfferRepository extends Repository
{

    function model()
    {
        return Offer::class;
    }
}