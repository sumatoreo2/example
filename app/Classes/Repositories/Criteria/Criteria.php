<?php


namespace App\Classes\Repositories\Criteria;


use App\Classes\Repositories\Eloquent\Repository;

abstract class Criteria
{
    abstract public function apply($model, Repository $repository);
}