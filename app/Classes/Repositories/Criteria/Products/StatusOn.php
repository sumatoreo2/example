<?php


namespace App\Classes\Repositories\Criteria\Products;


use App\Classes\Repositories\Criteria\Criteria;
use App\Classes\Repositories\Eloquent\Repository;

class StatusOn extends Criteria
{

    public function apply($model, Repository $repository) {
        $query = $model->where('status', '=', 1);

        return $query;
    }
}